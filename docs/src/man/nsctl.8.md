% NSCTL(8) Manage running l2tpns instance
% This manual page was written by Jonathan McDowell <noodles@the.earth.li>, for the Debian GNU/Linux system (but may be used by others)
% January 31, 2021

NAME
====

nsctl - manage running l2tpns instance

SYNOPSIS
========

**nsctl** \[**-d**\] \[**-h** *host*\[:*port*\]\] \[**-t** *timeout*\]
*command* \[*arg* \...\]

DESCRIPTION
===========

**nsctl** sends commands to a running **l2tpns** process. It provides
both for the loading or unloading of plugins and also the management of
sessions via functions provided by those plugins.

OPTIONS
=======

**-d** Enable debugging output

**-h *host*\[:*port*\]**
   The host running **l2tpns** that should receive the message. By
    default the message is sent to UDP port 1702 on **localhost**

**-t *timeout***
  Timeout in seconds to wait for a response from the server

COMMANDS
========

The first argument specifies the command to send to **l2tpns.** The
following commands are as defined:

**load\_plugin ***plugin*

:   Load the named *plugin*

**unload\_plugin ***plugin*

:   Unload the named *plugin*

**help**

:   Each loaded plugin is queried for what commands it supports and the
    synopsis for each is output

Any other value of *command* (and *args* if any) are sent to **l2tpns**
as-is, to be passed to each plugin which registers a **plugin\_control**
function in turn (in which it may be acted upon).

SEE ALSO
========

**l2tpns**(8)
