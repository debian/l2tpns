#!/usr/bin/env sh
# https://code.ffdn.org/l2tpns/l2tpns/-/issues/11

set -x
env
pwd

apt update -qqy
apt install -qqy build-essential libcli-dev git

make install
/usr/sbin/l2tpns -v || true

curr_version=$(grep -oP 'L2TPNS version *\K\S+' /var/log/l2tpns)
git_version=$(git describe --abbrev=0)
if [ "$curr_version" != "$git_version" ]; then
  echo "ERROR: L2TPNS version ($curr_version) is different than latest GIT version ($git_version)"
  exit 1
fi
